from pytest_mock import mocker
from palmoilextractor.engine.engine import Engine


def test_run_report_11(mocker):
    # product_name, report_id, month, quantity, region_name='NULL'
    engine = Engine(object)
    mocker.patch.object(engine, "_db_conn")
    mocker.patch.object(engine._db_conn, "stock")
    mocker.patch.object(engine._db_conn, "is_updater")
    engine._db_conn.is_updater.return_value = False
    mocker.patch.object(engine._db_conn, "is_inserter")
    engine._db_conn.is_inserter.return_value = True

    engine.run("tests/engine/201811.xls")
    

    engine._db_conn.stock.assert_has_calls([mocker.call("CRUDE PALM OIL", '11', "2018-01-01", 871984.0, region_name="PENINSULAR")])
    engine._db_conn.stock.assert_has_calls([mocker.call("CRUDE PALM KERNEL OIL", "11", "2018-03-01", 191815.0, region_name="NULL")])
    engine._db_conn.stock.assert_has_calls([mocker.call("PROCESSED PALM KERNEL OIL", "11", "2018-06-01", 127943.0, region_name="NULL")])
    engine._db_conn.stock.assert_has_calls([mocker.call("PROCESSED PALM OIL", "11", "2018-12-01", 1275796.0, region_name="NULL")])
