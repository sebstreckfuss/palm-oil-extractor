================
palmoilextractor
================


Processes Daily Averages and Monthly reports from MPOB


How to use
===========

To install this software, run the command ``python3 setup.py install``

This will install dependencies and add the module ``palmoilextractor`` to the python lib.

This means that you can import ``palmoilextractor`` modules from python anywhere

Next, to initialise the database, run ``python3 setup.py start``. This will run ``run.initial`` which creates and populates a database instance

Finally, use cron or equivalent to schedule when you would like the daily module to run

The daily module is ``palmoilextractor.run.main``

This module processes **both daily and monthly reports**

Settings
========

Options can be set at `palmoilextractor/src/palmoilextractor/config.json`

``login`` - the values for connecting to the MySQL Database

``temp_dir`` - temporary location for downloading files

``update_information`` - settings relating for running ``run.main``

    ``email`` - unused

    ``date`` - day of the month to process monthly reports. Valid options within the range [1,28], inclusive

    ``sendDaily``/``sendMonthly`` - unused

    ``offset`` - Number of days `previous` to the current date to process reports.

        ``offset: 0`` = no change

        ``offset: 1`` = previous day

Note
====

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.

