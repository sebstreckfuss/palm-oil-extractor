from json import load, dump
from datetime import datetime


def encode(clear, key="insecure encryption only for obscurity, not security"):
    """Only to be used to prevent simply reading the password from the config file"""
    # Removed
    # enc = []
    # for i in range(len(clear)):
    #     key_c = key[i % len(key)]
    #     enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
    #     enc.append(enc_c)
    # return "".join(enc)
    return clear


def decode(enc, key="insecure encryption only for obscurity, not security"):
    # Removed
    # dec = []
    # for i in range(len(enc)):
    #     key_c = key[i % len(key)]
    #     dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
    #     dec.append(dec_c)
    # return "".join(dec)
    return enc


def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%s' % (prefix, bar, iteration, suffix), end = '\r')


def genTime():
    for hour in range(24):
        hour = str(hour)
        if len(hour)==1:
            hour = "0" + str(hour)
        for mins in range(12):
            m = str(mins * 5)
            if len(m) == 1:
                m = "0" + m
            yield (hour, m)


def getHours24():
    for hour in range(24):
        hour = str(hour)
        if len(hour)==1:
            hour = "0" + hour
        yield hour


def getHours12():
    for suffix in ["AM", "PM"]:
        for hour in [12] + [j for j in range(1,12)]:
            yield str(hour), suffix


def getMins():
    for mins in range(12):
        m = str(mins*5)
        if len(m)==1:
            m = "0" + m
        yield m


def recursive_in(target, x):
    for x1 in x:
        if hasattr(x1, "__iter__") and not isinstance(x1, str):
            if recursive_in(target, x1):
                return recursive_in(target, x1)
        elif x1.upper() == target:
            return x1
    return False


def refresh_config():
    with open("default.json") as source:
        tmp = load(source)
    with open("config.json", "w") as dest:
        dump(tmp, dest)


def test_exception_conditions(func, attr, val, exception=Exception):
    ex = None
    if attr and not hasattr(func, attr):
        return False
    try:
        func()
    except exception as e:
        ex = e
    if attr and hasattr(ex, attr):
        return getattr(ex, attr)==val
    elif ex:
        return True
    return False


class Writer:
    TIME = None
    LINES = []

    def __init__(self, path):
        self.path = path
        self.TIME = datetime.now().strftime("%H:%M:%S")

        with open(self.path, "a+") as file:
            file.write("\nTime: %s\n" % (self.TIME,))

    def save(self):
        with open(self.path, "a+") as file:
            for msg in self.LINES:
                file.write(str(msg) + "\n")

    def write(self, msg):
        self.LINES.append(msg)
