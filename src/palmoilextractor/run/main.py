from palmoilextractor.database.database import Database
from palmoilextractor.engine.processors import DateFormat as FullDate

from json import load
import time
from datetime import datetime, timedelta, date as _get_date

from palmoilextractor.run.daily import main as getDaily
from palmoilextractor.run.monthly import main as monthlyUpdate


class EmailPlaceholder:
    def __init__(self, *args, **kwargs):
        """Placeholder object for sending emails"""
    def send(self, subject, body):
        print(subject)
        print(body)


def main():
    with open("config.json") as j:
        config = load(j)
    print("Getting daily prices...")
    date = datetime(int(time.strftime("%Y")), int(time.strftime("%m")), int(time.strftime("%d")))
    date -= timedelta(days=config["update-information"]["offset"])
    date = (date.year, date.month, date.day)
    getDaily(givenDate=date)
    print("Retrieving new daily averages...")
    db = Database(
        config["login"]["host"], 
        config["login"]["port"], 
        config["login"]["user"], 
        config["login"]["password"])
    dateParser = FullDate(None)

    avg = db.query("""
        SELECT tenor, AVG(average_amount) 
        FROM daily_average 
        WHERE updated LIKE '%s-__'
        GROUP BY tenor
        """ % dateParser.process(tuple(date[0:2])[::-1])[0:7])

    runMonth = config["update-information"]["date"] == date[2]

    notify = [config["update-information"]["sendDaily"], config["update-information"]["sendMonthly"]]

    if runMonth:
        print("Getting monthly updates...")
        monthlyUpdate(forceUpdate=date)

    emailer = EmailPlaceholder()
    subject = ""
    if notify[0]:
        subject += "Daily reports have been processed"
    if runMonth and notify[1]:
        if notify[0]:
            subject += " and monthly reports now available"
        else:
            subject += "Monthly reports are now available"
    body = ""
    if notify[0]:
        body += "The averages for this month as of {date} are as follows:\n\t{t1}:\t{a1}\n\t{t2}:\t{a2}\n\t{t3}:\t{a3}\n\t{t4}:\t{a4}".format(
            t1=avg[0][0],
            t2=avg[1][0],
            t3=avg[2][0],
            t4=avg[3][0],
            a1=avg[0][1],
            a2=avg[1][1],
            a3=avg[2][1],
            a4=avg[3][1],
            date=_get_date(*date).strftime("%d/%m/%Y"))
    if all(notify):
        body += "\n"
    if runMonth and notify[1]:
        body += "This month's reports have been processed and can now be viewed in the database"
    print("\nEMAIL WILL LOOK LIKE THIS\n\n")
    if any(notify):
        emailer.send(subject, body)


if __name__=="__main__":
    main()