from unittest.mock import Mock
from json import load
from os import listdir
import re

from palmoilextractor.engine.engine import Engine
from palmoilextractor.util import Writer


class TestSet:
    DB_CONN = Mock()
    ENGINE = Engine(DB_CONN)
    with open("src/palmoilextractor/config.json") as file:
        CONFIG = load(file)
    with open("src/palmoilextractor/engine/config.json") as file:
        ENGINE_CONFIG = load(file)
    LOGIN_INFO = [*CONFIG["login"].values()]

    @classmethod
    def test_all(cls, path):
        files = [
            path+"/"+file for file in listdir(path=path) if re.findall("[0-9]{6,7}\.xls", file)
        ]
        for file in files:
            cls.ENGINE.run(file)

    @classmethod
    def test_year(cls, path, year):
        files = [
            path+"/"+file for file in listdir(path=path) if re.findall(str(year)+"[0-9]{2,3}\.xls", file)
        ]
        for file in files:
            cls.ENGINE.run(file)

    @classmethod
    def test_report(cls, report_path):
        cls.ENGINE.run(report_path)

    @classmethod
    def test_report_id(cls, path, report_id):
        files = [
            path+"/"+file for file in listdir(path=path) if re.findall("[0-9]{4}"+str(report_id)+"\.xls", file)
        ]
        for file in files:
            cls.ENGINE.run(file)

    @classmethod
    def dump(cls, action=print):
        for call in cls.DB_CONN.mock_calls:
            action(call)
        cls.DB_CONN = Mock()


def main():
    writer = Writer("out.txt")
    TestSet.test_report_id("./reports", 11)
    TestSet.dump(writer.write)
    writer.save()


if __name__ == "__main__":
    main()
