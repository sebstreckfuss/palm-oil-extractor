from palmoilextractor.daily.getter import SoupGetter
from palmoilextractor.database.database import Database
from palmoilextractor.util import decode, refresh_config

from json import load
import time
from datetime import datetime, timedelta


def main(givenDate=None):
    print("Getting daily report (module)")

    try:
        with open("config.json") as file:
            tmp = load(file)
            host = tmp["login"]["host"]
            port = tmp["login"]["port"]
            user = tmp["login"]["user"]
            password = decode(tmp["login"]["password"])
    except FileNotFoundError:
        refresh_config()
        with open("config.json") as file:
            tmp = load(file)
            host = tmp["login"]["host"]
            port = tmp["login"]["port"]
            user = tmp["login"]["user"]
            password = decode(tmp["login"]["password"])

    if not givenDate:
        date = datetime(int(time.strftime("%Y")), int(time.strftime("%m")), int(time.strftime("%d")))
        with open("config.json") as file:
            date -= timedelta(days=load(file)["update-information"]["offset"])
        date = (date.year, date.month, date.day)
    else:
        date = givenDate

    db_conn = Database(host, port, user, password)
    sg = SoupGetter(db_conn.insert.daily_average)
    sg.run(date[0], date[1])
    db_conn.connection.close()


if __name__ == "__main__":
    main()