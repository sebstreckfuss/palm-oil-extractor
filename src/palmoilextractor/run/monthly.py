from palmoilextractor.database.database import Database
from palmoilextractor.engine.engine import Engine
from palmoilextractor.get.getter import Getter

from palmoilextractor.util import decode, refresh_config
from os import path

import argparse
from time import strftime as date
from json import load, dump


def main(forceUpdate=None):
    """forceUpdate is None or a tuple ()"""
    with open("engine/config.json") as file:
        targets = [i for i in load(file).keys()]

    if not forceUpdate:
        parser = argparse.ArgumentParser(description="Run the engine and print the results")
        parser.add_argument("--gui", dest="gui", action="store_true", default=False, help="use gui to connect to database")

        subparser = parser.add_subparsers(help="report origin", dest="method")

        update = subparser.add_parser("update", help="update existing entries for the given dates")
        update.add_argument("-y", type=int, help="specify year. Leave out for current year", default=int(date("%Y")))
        update.add_argument("-m", type=int, help="specify month. Leave out for current month", default=int(date("%m")))
        update.add_argument("--temp", type=str, default="reports/", help="specify temporary directory to create")
        update.add_argument("--no_rm", dest="leave", default=True, action="store_false", help="don't delete the temp dir after completing")
        update.add_argument("-o", dest="override", action="store_true", default=False, help="replace the indicated directory if it exists")


        _dir = subparser.add_parser("dir", help="use existing directory of reports")
        _dir.add_argument("location", help="existing directory of reports")
        dir_year = _dir.add_mutually_exclusive_group(required=True)
        dir_year.add_argument("--year", type=int, help="specify year")
        dir_year.add_argument("--range", type=int, nargs=2, help="specify year range")

        get = subparser.add_parser("get", help="retrieve reports online")
        get_year = get.add_mutually_exclusive_group(required=True)
        get_year.add_argument("--year", type=int, help="specify year")
        get_year.add_argument("--range", type=int, nargs=2, help="specify year range")
        get.add_argument("--temp", type=str, default="reports/", help="specify temporary directory to create")
        get.add_argument("--no_rm", dest="leave", default=True, action="store_false", help="don't delete the temp dir after completing")
        get.add_argument("-o", dest="override", action="store_true", default=False, help="replace the indicated directory if it exists")


    with open("engine/config.json") as file:
        config = load(file)
    try:
        args = parser.parse_args()
    except NameError:
        pass


    if not forceUpdate:
        if args.method == "dir" and not path.exists(args.location):
            _dir.print_help()
            exit(print("\nDirectory doesn't exist"))

        if args.gui:
            from _tkinter import TclError
            from tkgui.login import Login
            login_prompt = Login()
            try:
                host, port, user, password = login_prompt.get()
            except TclError as t:
                raise t
                exit()
    if forceUpdate or args.method:
        try:
            with open("config.json") as file:
                tmp = load(file)
                host = tmp["login"]["host"]
                port = tmp["login"]["port"]
                user = tmp["login"]["user"]
                password = decode(tmp["login"]["password"])
        except FileNotFoundError:
            refresh_config()
            with open("config.json") as file:
                tmp = load(file)
                host = tmp["login"]["host"]
                port = tmp["login"]["port"]
                user = tmp["login"]["user"]
                password = decode(tmp["login"]["password"])

    if forceUpdate:
        db_conn = Database(host, port, user, password, date=(forceUpdate[0], forceUpdate[1]))
        e = Engine(db_conn.update)
        g = Getter(initial_dir=tmp["temp_dir"], override=True)
        g.get_year(forceUpdate[0])
        for target in config:
            e.run(tmp["temp_dir"]+str(forceUpdate[0])+target+".xls")
        g.close()   

    elif args.method == "update":
        db_conn = Database(host, port, user, password, date=(args.y, args.m))
        e = Engine(db_conn.update)
        g = Getter(initial_dir=args.temp, override=args.override)
        g.get_year(args.y)
        for target in config:
            e.run(args.temp+str(args.y)+target+".xls")
        if args.leave:
            g.close()     

    elif args.method == "get":
        db_conn = Database(host, port, user, password)
        e = Engine(db_conn.insert)
        g = Getter(initial_dir=args.temp, override=args.override)
        if args.range:
            for year in range(args.range[0], args.range[1]+1):
                g.get_year(year)
                for target in config:
                    e.run(args.temp+str(year)+target+".xls")

        elif args.year:
            g.get_year(args.year)
            for target in config:
                    e.run(args.temp+str(args.year)+target+".xls")
        if args.leave:
            g.close()
    elif args.method == "dir":
        if not path.exists(args.location):
            parser.print_help()
            exit(print("\nDirectory doesn't exist"))
        db_conn = Database(host, port, user, password)
        e = Engine(db_conn.insert)
        if args.range:
            for year in range(args.range[0], args.range[1]+1):
                for target in config:
                    e.run(args.location+str(year)+target+".xls")
        elif args.year:
            for target in config:
                print(target)
                e.run(args.location+str(args.year)+target+".xls")
    db_conn.connection.close()

if __name__ == "__main__":
    main()