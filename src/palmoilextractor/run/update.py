from palmoilextractor.database.database import Database
from palmoilextractor.get.getter import Getter
from palmoilextractor.engine.engine import Engine
from palmoilextractor.daily.getter import SoupGetter

from datetime import date
from palmoilextractor.util import refresh_config, decode
from json import load, dump
from os import path, mkdir

def main():

	print("Please input the date you would like to start the engine in the form year, month, day, with spaces in between each:")
	s_raw = input("\"YYYY MM DD\":\t")
	if not s_raw:
		exit(print("Please input a start date"))
	s = s_raw.split(" ")
	if len(s)!=3:
		exit(print("Invalid argument: %s" % s_raw))
	try:
		s = [int(c) for c in s]
	except ValueError:
		exit(print("Invalid argument: %s" % s_raw))


	print("Please input the date you would like to run the engine to in the form year, month, day, with spaces in between each:")
	e_raw = input("\"YYYY MM DD\"(leave blank for today):\t")
	if e_raw:
		e = e_raw.split(" ")
		if len(e)!=3:
			exit(print("Invalid argument: %s" % e_raw))
		try:
			e = [int(c) for c in e]
		except ValueError:
			exit(print("Invalid argument: %s" % e_raw))
	else:
		e_raw = date.today().strftime("%Y %m %d")
		e = [int(c) for c in e_raw.split(" ")]

	if not path.exists("config.json"):
		if input("Could not find config file, would you like to make a default one? (Y/N):\t") not in ["y","Y","yes","Yes"]:
			exit("Cancelled by user")
		refresh_config()

	with open("engine/config.json") as j:
		config = load(j)

	with open("config.json") as j:
		tmp = load(j)
		temp_dir = tmp["temp_dir"]
		user_info = tmp["login"]
		user_info["password"] = decode(user_info["password"])

	db_conn = Database(*list(user_info.values()))
	try:
		getter = Getter(initial_dir=temp_dir)
	except FileExistsError:
		print("%s already exists!\n\t1: Cancel\n\t2: Overwrite\n\t3: Choose a different directory")
		ans = input()
		if ans not in ["1", "2", "3"]:
			ans = 1
		else: ans = int(ans)
		if ans == 1:
			exit()
		elif ans == 2:
			getter = Getter(initial_dir=temp_dir, override=True)
		else:
			loc = input("Directory:\t")
			mkdir(loc)
			getter = Getter(initial_dir=loc)
		main_engine = Engine(db_conn.insert)
		daily_getter = SoupGetter(db_conn.insert.daily_average)

		for year in range(s[0], e[0]+1):
			getter.get_year(year)
			for target in config:
				if "loc" in locals():
					main_engine.run(loc+str(year)+target+".xls")
				else:
					main_engine.run(temp_dir+str(year)+target+".xls")
			if year == s[0]:
				for month in range(s[1], 13):
					daily_getter.run(year, month)
			elif year==e[0]:
				for month in range(1, e[1]):
					daily_getter.run(year, month)
			else:
				for month in range(1, 13):
					daily_getter.run(year, month)
		print("Done")

if __name__ == "__main__":
	main()