from palmoilextractor.database.database import Database
from palmoilextractor.engine.engine import Engine
from palmoilextractor.daily.getter import SoupGetter
from palmoilextractor.get.getter import Getter
from palmoilextractor.util import refresh_config, decode


from json import load, dump
import time
from os import path


def main():
	if not path.exists("config.json"):
		refresh_config()

	with open("engine/config.json") as file:
		config = load(file)
	with open("config.json") as file:
		temp_dir = load(file)["temp_dir"]

	with open("config.json") as file:
		tmp = load(file)
		host = tmp["login"]["host"]
		port = tmp["login"]["port"]
		user = tmp["login"]["user"]
		password = decode(tmp["login"]["password"])

	db_conn = Database(host, port, user, password)
	getter = Getter(initial_dir=temp_dir, override=True)
	main_engine = Engine(db_conn.insert)
	daily_engine = SoupGetter(db_conn.insert.daily_average)

	curr_year = int(time.strftime("%Y"))
	curr_month = int(time.strftime("%m"))
	for year in range(2006, curr_year+1):
		getter.get_year(year)
		for target in config:
			main_engine.run(temp_dir+str(year)+target+".xls")
		if year != curr_year:
			for month in range(1, 13):
				daily_engine.run(year, month)
		else:
			for month in range(1, curr_month+1):
				daily_engine.run(year, month)


if __name__ == "__main__":
	main()