from palmoilextractor.engine.phases import Processor
from json import load
from palmoilextractor.util import recursive_in

class DateFormat(Processor):
    def process(self, data):
        _MONTHS = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        assert type(data)==tuple and len(data)==2
        month, year= data
        year = int(year)
        if type(month)!=str:
            if len(str(month))==2:
                return "{0}-{1}-01".format(year, month)
            else:
                return "{0}-0{1}-01".format(year, month)
        for m in _MONTHS:
            if m in month.upper():
                if len(str(_MONTHS.index(m)+1))==2:
                    return "{0}-{1}-01".format(year, _MONTHS.index(m) + 1)
                else:
                    return "{0}-0{1}-01".format(year, _MONTHS.index(m) + 1)

class ProductFormat(Processor):
    def process(self, data):
        with open("engine/glossary.json") as file:
            _PRODUCTS = load(file)
        if recursive_in(data.upper(), _PRODUCTS):
            return recursive_in(data.upper(), _PRODUCTS)
        else:
            print(data + " is missing")
        return data

    

class DateFromFilename(Processor):
    def process(self, data):
        _MONTHS = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        assert type(data)==tuple and len(data)==2
        month, year= data
        year = int(year)
        if type(month)!=str:
            if len(str(month))==2:
                return "{0}-{1}-01".format(year, month)
            else:
                return "{0}-0{1}-01".format(year, month)
        for m in _MONTHS:
            if m in month.upper():
                if len(str(_MONTHS.index(m)+1))==2:
                    return "{0}-{1}-01".format(year, _MONTHS.index(m) + 1)
                else:
                    return "{0}-0{1}-01".format(year, _MONTHS.index(m) + 1)

class DateWithExclusion(Processor):
    def process(self, data):
        _MONTHS = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        assert type(data)==tuple and len(data)==3
        ex = self._cfg["headerExclude"]
        date = []
        for d in data:
            if data.index(d) == ex:
                continue
            date.append(d)
        month, year = date
        year = int(year)
        if type(month)!=str:
            if len(str(month))==2:
                return "{0}-{1}-01".format(year, month)
            else:
                return "{0}-0{1}-01".format(year, month)
        for m in _MONTHS:
            if m in month.upper():
                if len(str(_MONTHS.index(m)+1))==2:
                    return "{0}-{1}-01".format(year, _MONTHS.index(m) + 1)
                else:
                    return "{0}-0{1}-01".format(year, _MONTHS.index(m) + 1)

class FullDate(Processor):
    def process(self, data):
        _MONTHS = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        day, month, year= data
        year = int(year)
        for m in _MONTHS:
            if isinstance(month, str) and m in month.upper():
                m_ = str(_MONTHS.index(m)+1)
                break
            m_ = str(month)
        if len(m_)==1:
            m_ = "0" + m_
        d_ = str(day)
        if len(d_)==1:
            d_ = "0" + d_
        return "{0}-{1}-{2}".format(year, m_, d_)

class AllowedValueProcessor(Processor):
    def process(self, data):
        cfgData = self._cfg["kwargs_processors"][data[0]]
        print(f"        cfgData = {cfgData}")
        data = data[1]
        if data in cfgData[1]:
            print(f"        returning {data}")
            return data
        else:
            print("        returning \"NULL\"")
            return "NULL"