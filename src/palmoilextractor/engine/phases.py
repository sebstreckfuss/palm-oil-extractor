from abc import ABC, abstractmethod


class TableIdentifier(ABC):

    def __init__(self, cfg):
        self._cfg = cfg

    @abstractmethod
    def identify_tables(self, filename):
        """Return table metadata for an excel file

        The return type should have the form
        [
            {
                "header_rows": <tuple of header row indicies>,
                "nrows": <number of rows in the table>
                "index_col": <the position of the index column>,
            },
            ...
        ]
        """
        ...



class DataGatherer(ABC):

    def __init__(self, cfg):
        self._cfg = cfg

    @abstractmethod
    def gather_data(self, filename, table_data): ...


class ArgConstructor(ABC):

    def __init__(self, cfg):
        self._cfg = cfg

    @abstractmethod
    def construct_args(self, row_data, report_id): ...


class Processor(ABC):

    def __init__(self, cfg):
        self._cfg = cfg

    @abstractmethod
    def process(self, data): ...

