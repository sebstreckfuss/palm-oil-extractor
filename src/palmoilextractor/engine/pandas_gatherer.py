from palmoilextractor.engine.phases import DataGatherer
from os import path
import palmoilextractor.pre_xlrd
from pandas import DataFrame, read_excel 
import re
import math

class PandasGatherer(DataGatherer):

    def gather_data(self, filename, table_data):

        _check_val = any("allowedValue" in _item.values() for _item in self._cfg["filters"])
        if self._cfg["use_index_col"]:
            df = read_excel(
                filename, header=table_data["header_rows"],
                nrows=table_data["nrows"],
                index_col=table_data["index_col"],
                na_values=["", " "],
            )
        else:
            df = read_excel(
                filename, header=table_data["header_rows"],
                nrows=table_data["nrows"],
                na_values=["", " "],
            )
        data = []
        for column in df:
            prev_index = df.index.name
            for index, cell_data in df[column].items():
                try:
                    if path.split(filename)[1][4:] == "11.xls" and math.isnan(cell_data):
                        prev_index = index
                except Exception as e:
                    print(cell_data)
                    raise e
                if not self._is_filtered(column, index, cell_data, filename):
                    data.append({"header": column, "index": index,
                        "data": cell_data, "previousEmptyIndex": prev_index})
        return data

    def _is_filtered(self, column, index, cell_data, filename):
        if "filters" in self._cfg:
            for filter_ in self._cfg["filters"]:
                ft = filter_["type"]
                if "values" in filter_: values = filter_["values"]
                if ft == "header" and any(re.match(v, column) for v in values):
                    return True
                if ft == "index":
                    if not isinstance(index, str):
                        return True
                    if any(re.match(v, index) for v in values):
                        return True
                if ft == "headerItem" and any(re.match(v, column[filter_["item"]]) for v in values):
                    return True
                if ft == "indexItem" and any(re.match(v, index[filter_["item"]]) for v in values):
                    return True
                if ft == "headerItemYearMismatch" and int(column[filter_["item"]]) != int(path.split(filename)[1][:4]):
                    return True
                if ft == "headerVar":
                    year = int(path.split(filename)[1][:4])
                    for vals in filter_["values"]:
                        if column in vals[0] % (year + vals[1]):
                            return True
                if ft == "indexType" and repr(type(index)) in ["<class '%s'>" % i for i in filter_["values"]]:
                    return True
                if ft == "headerItem_re" and any(re.match("^"+i+"$", column[filter_["item"]]) for i in values):
                    return True
                if ft == "index_re" and any(re.match("^"+i+"$", index) for i in values):
                    return True
                if ft == "cellnan" and math.isnan(cell_data):
                    return True

        return False

                
