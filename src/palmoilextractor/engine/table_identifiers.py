import palmoilextractor.pre_xlrd
from xlrd import open_workbook

from palmoilextractor.engine.phases import TableIdentifier

class IterateIdentifier(TableIdentifier):
    def identify_tables(self, filename):
        return self.iter_table(filename, iterations=self._cfg["ntables"])

    def iter_table(self, filepath, iterations=1):
        target = open_workbook(filepath).sheet_by_index(0)
        last = (0, self._cfg["start"])
        data = []
        for i in range(iterations):
            new = {}
            last = self.get_rows(target, last[1])
            new["nrows"] = len(range(*last))
            new["header_rows"] = self.get_header(target, last[0])
            new["index_col"] = self._cfg["index_col"]
            data.append(new)
        return data

    def get_rows(self, target, start):
        """Returns a tuple that contains (first data row, last data row)"""
        rows = target.nrows
        empty = False               # flag to control the for loop
        relevent = False                            # says if the table has started yet
        subsequent = start != self._cfg["start"]    # says if this is after the first iteration
        for row in range(start, rows):
            has_data = target.row(row)[0].ctype
            if has_data and subsequent:
                relevent = True
            if (not has_data and not subsequent) or (subsequent and relevent and not has_data):
                empty = True
            elif empty:
                first_row = row
                break
        empty = False
        for r in range(first_row, rows):
            if empty: break
            row = target.row(r)
            for c in row:
                if not c.ctype:
                    empty = True
                    break
            last_row = r
        return (first_row, last_row)

    def get_header(self, target, start):
        return [start-i-1 for i in range(self._cfg["headers"])][::-1]


class SingleIdentifier(TableIdentifier):
    def identify_tables(self, filename):
        target = open_workbook(filename).sheet_by_index(0)
        return self.read_table(target)

    def read_table(self, target):
        rows = []
        for col in enumerate(target.col_values(0)):
            if col[1]:
                rows.append(col)
        last = None
        header = None
        for i in rows:
            if last == None:
                last = i[0]
                continue
            if i[0] == last + 1:
                last = i[0]
                continue
            elif not header:
                header = i[0]
                last = i[0]
                continue
        end = i[0]
        header_len = self._cfg["headers"]
        data = {
            "nrows": len(range((header-1), end)),
            "header_rows": [header-i-1 for i in range(header_len)][::-1],
            "index_col": self._cfg["index_col"]
        }
        return [data]

class SingleIdentifierRevHeader(TableIdentifier):
    def identify_tables(self, filename):
        target = open_workbook(filename).sheet_by_index(0)
        return self.read_table(target)

    def read_table(self, target):
        rows = []
        for col in enumerate(target.col_values(0)):
            if col[1]:
                rows.append(col)
        last = None
        header = None
        for i in rows:
            if last == None:
                last = i[0]
                continue
            if i[0] == last + 1:
                last = i[0]
                continue
            elif not header:
                header = i[0]
                last = i[0]
                continue
        end = i[0]
        header_len = self._cfg["headers"]
        data = {
            "nrows": len(range((header+header_len+1), end)),
            "header_rows": [header-i-1 for i in range(header_len)],
            "index_col": self._cfg["index_col"]
        }
        return [data]

class MultiCol(TableIdentifier):
    def identify_tables(self, filename):
        target = open_workbook(filename).sheet_by_index(0)
        return self.read_table(target)

    def read_table(self, target):
        start = self._cfg["start"]
        data = {
            "nrows": target.nrows - self._cfg["start"],
            "header_rows": start,
            "index_col": [self._cfg["index_col"] + i for i in range(self._cfg["columns"])]
        }
        return [data]

class TopLeftIndexed(TableIdentifier):
    def identify_tables(self, filename):
        target = open_workbook(filename).sheet_by_index(0)
        return self.read_table(target, iterations=self._cfg["ntables"])

    def read_table(self, target, iterations=1):
        data = []
        new = {}
        new["header_rows"] = [r for r in range(self._cfg["start"], self._cfg["start"]+self._cfg["headers"])]
        new["nrows"] = self.get_rows(target, new["header_rows"][-1]+1)
        new["index_col"] = self._cfg["index_col"]
        data.append(new)
        for i in range(1, iterations):
            data.append(self.read_subsequent(target, new["header_rows"][-1]+new["nrows"]+1))
            new = data[-1]
        return data

    def get_rows(self, target, start):
        """start is the first data cell"""
        rows = [r for r in target.get_rows()][start:target.nrows]
        n = 0
        for row in rows:
            if not row[0].value:
                break
            n += 1
        return n


    def read_subsequent(self, target, start):
        s = self.find_gap(target, start)
        new = {}
        new["header_rows"] = [r for r in range(s, s+self._cfg["headers"])]
        new["nrows"] = self.get_rows(target, new["header_rows"][-1]+1)
        new["index_col"] = self._cfg["index_col"]
        return new

    def find_gap(self, target, start):
        rows = [r for r in target.get_rows()][start:target.nrows]
        for row in rows:
            c = row[0]
            if c.value:
                return rows.index(row)+start+2

class CombinedIndex(TableIdentifier):
    def identify_tables(self, filename):
        target =open_workbook(filename).sheet_by_index(0)
        return self.read(target)

    def read(self, target):
        data = []
        skip = 2
        firsttitle = 0


        for i in range(self._cfg["ntables"]):
            new = {}
            new["header_rows"] = [i for i in range(firsttitle+skip, firsttitle+skip+self._cfg["headers"])]
            new["nrows"] = 13
            new["index_col"] = self._cfg["index_col"]
            if i != self._cfg["ntables"]-1:
                for ind, r  in enumerate([j for j in target.get_rows()][18:], 18):
                    if r[0].ctype:
                        firsttitle = ind
                        break
            data.append(new)

        return data
