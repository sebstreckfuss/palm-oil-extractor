import json
import copy
import os
import re
from sys import argv
from unittest.mock import Mock

from palmoilextractor.engine.table_identifiers import IterateIdentifier, SingleIdentifier, SingleIdentifierRevHeader, MultiCol, TopLeftIndexed, CombinedIndex
from palmoilextractor.engine.pandas_gatherer import PandasGatherer
from palmoilextractor.engine.basic_arg_constructor import BasicArgConstructor, UpdateArgConstructor
from palmoilextractor.engine.processors import DateFormat, ProductFormat, DateWithExclusion, AllowedValueProcessor

from palmoilextractor.database.update import Updater
from palmoilextractor.database.inserter import Inserter


class Engine:
    CONFIG_LOCATION = "src/palmoilextractor/engine/config.json"
    DEFAULTS_LOCATION = "src/palmoilextractor/engine/defaults.json"

    # Dictionary of possible choices for each phase
    TABLE_IDENTIFIERS = {
        "iterate_identifier": IterateIdentifier,
        "single": SingleIdentifier,
        "single_reversed_header": SingleIdentifierRevHeader,
        "multi_col": MultiCol,
        "topLeftIndexed": TopLeftIndexed,
        "mixed_index": CombinedIndex
    }
    DATA_GATHERERS = {
        "pandas_gatherer": PandasGatherer
    }
    ARG_CONSTRUCTORS = {
        "basic_arg_constructor": BasicArgConstructor,
    }
    PROCESSORS = {
        "date": DateFormat,
        "product": ProductFormat,
        "dateFromFilename": DateFormat,
        "dateExclusion": DateWithExclusion,
        "allowedValueProcessor": AllowedValueProcessor
    }

    def __init__(self, db_conn, location=argv[0]):
        with open(self.CONFIG_LOCATION) as f:
            self._config = json.load(f)
        self._db_conn = db_conn
        self.location = location

        if isinstance(db_conn, Updater):
            self.ARG_CONSTRUCTORS["basic_arg_constructor"] = UpdateArgConstructor

        with open(self.DEFAULTS_LOCATION) as f:
            self._defaults = json.load(f)

    def _get_report_config(self, report_id):
        cfg = copy.deepcopy(self._defaults)
        self._merge(self._config[report_id], cfg)
        return cfg


    def _merge(self, source, dest):
        for key, value in source.items():
            if isinstance(value, dict):
                # get node or create one
                node = dest.setdefault(key, {})
                self._merge(value, node)
            else:
                dest[key] = value

        return dest

    def identify_tables(self, cfg, report_id, report_filename):
        identifier = self.TABLE_IDENTIFIERS[cfg["table_identifier"]](cfg)
        return identifier.identify_tables(report_filename)

    def gather_data(self, cfg, report_filename, all_table_data):
        gatherer = self.DATA_GATHERERS[cfg["data_gatherer"]](cfg)

        data = []
        for table_data in all_table_data:
            data += gatherer.gather_data(report_filename, table_data)
        return data

    def construct_args(self, cfg, data, report_id):
        constructor = self.ARG_CONSTRUCTORS[cfg["arg_constructor"]](cfg)
        return [constructor.construct_args(row, report_id) for row in data]

    def run(self, report_filename, update=None):
        pattern = "(?<=/.{4})[0-9]{2,3}(?=.xls)"
        report_id = re.findall(pattern, report_filename)[0]
        cfg = self._get_report_config(report_id)
        all_table_data = self.identify_tables(cfg, report_id, report_filename)
        try:
            data = self.gather_data(cfg, report_filename, all_table_data)
            if not data:
                print(report_filename + " is broken!")
        except TypeError as e:
            print(report_filename)
            raise e
        data = self.construct_args(cfg, data, report_id)

        if self._db_conn.is_inserter():
            for i, proc in cfg["args_processors"].items():
                if proc == "dateFromFilename":
                    for args, _ in data:
                        args[int(i)] = self.PROCESSORS[proc](cfg).process((
                            args[int(i)], 
                            os.path.split(report_filename)[1][:4]))
                else:
                    for args, _ in data:
                        args[int(i)] = self.PROCESSORS[proc](cfg).process(args[int(i)])

            for i, proc in cfg["kwargs_processors"].items():
                for _, kwargs in data:
                    kwargs[i] = self.PROCESSORS[proc[0]](cfg).process([i, kwargs[i]])
        elif self._db_conn.is_updater():
            for i, proc in cfg["args_processors"].items():
                if proc == "dateFromFilename":
                    for args, kwargs in data:
                        kwargs[list(kwargs)[int(i)]] = self.PROCESSORS[proc](cfg).process((
                            kwargs[list(kwargs)[int(i)]], 
                            os.path.split(report_filename)[1][:4]))
                else:
                    for args, kwargs in data:
                        kwargs[list(kwargs)[int(i)]] = self.PROCESSORS[proc](cfg).process(
                            kwargs[list(kwargs)[int(i)]])

            for i, proc in cfg["kwargs_processors"].items():
                # i is the kwarg name, proc is an array of length 2 where the
                # first element is the processor name, and the second argument
                # is the ... TBC
                for args, kwargs in data:
                    kwargs[i] = self.PROCESSORS[proc[0]](cfg).process([i, kwargs[i]])


        attr = getattr(self._db_conn, cfg["method"])
        for row in data:
            if isinstance(self._db_conn, Inserter) or isinstance(self._db_conn, Mock):
                attr(*row[0], **row[1])
            elif isinstance(self._db_conn, Updater):
                attr(**row[1])