from palmoilextractor.engine.phases import ArgConstructor
from json import load

class BasicArgConstructor(ArgConstructor):

    def construct_args(self, row_data, report_id):
        args = [self._resolve_arg(arg_format, row_data, report_id)
                for arg_format in self._cfg["args"]]
        kwargs = {k: self._resolve_arg(arg_format, row_data, report_id)
                  for k, arg_format in self._cfg["kwargs"].items()}
        return args, kwargs

    def _resolve_arg(self, arg_format, row_data, report_id):

        arg_type = arg_format["from"]
        result = None
        
        if arg_type == "index":
            result = row_data["index"]
        elif arg_type == "header":
            result = row_data["header"]
        elif arg_type == "value":
            result = row_data["data"]
        elif arg_type == "previousEmptyIndex":
            result = row_data["previousEmptyIndex"]
        elif arg_type == "headerItem":
            result = row_data["header"][arg_format["item"]]
        elif arg_type == "indexItem":
            result = row_data["index"][arg_format["item"]]
        elif arg_type == "report_id":
            result = report_id
        elif arg_type == "fixed":
            result = arg_format["value"]

        if "alternate" in arg_format:
            alternate = arg_format["alternate"]
            if result in alternate["blocked"]:
                result = self._resolve_arg(alternate, row_data, report_id)

        return result

class UpdateArgConstructor(ArgConstructor):
    def __init__(self, cfg):
        super().__init__(cfg)
        with open("database/update_template.json") as j:
            self.templates = load(j)

    def construct_args(self, row_data, report_id):
        args = [self._resolve_arg(arg_format, row_data, report_id)
                for arg_format in self._cfg["args"]]
        kwargs = {k: self._resolve_arg(arg_format, row_data, report_id)
            for k, arg_format in zip(self.templates["kwargs"][self._cfg["method"]], self._cfg["args"])}
        for k, arg_format in self._cfg["kwargs"].items():
            kwargs[k] = self._resolve_arg(arg_format, row_data, report_id)
        return args, kwargs

    def _resolve_arg(self, arg_format, row_data, report_id):
            arg_type = arg_format["from"]
            
            if arg_type == "index":
                return row_data["index"]
            elif arg_type == "header":
                return row_data["header"]
            elif arg_type == "value":
                return row_data["data"]
            elif arg_type == "headerItem":
                return row_data["header"][arg_format["item"]]
            elif arg_type == "indexItem":
                return row_data["index"][arg_format["item"]]
            elif arg_type == "report_id":
                return report_id
            elif arg_type == "fixed":
                return arg_format["value"]
