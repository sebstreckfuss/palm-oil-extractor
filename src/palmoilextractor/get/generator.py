from json import load

class UrlGenerator:
	def __init__(self):
		self.URL = "http://bepi.mpob.gov.my/stat/web_report1.php?val=%s%s&excel=Y"
		with open("engine/config.json") as f:
			self.reports = list(load(f).keys())

	def get_year(self, year):
		urls = []
		for report in self.reports:
			urls.append((self.URL % (year, report), "%s%s.xls" % (year, report)))
		return urls

	def get_year_range(self, start, stop):
		urldict = {}
		for year in range(start, stop + 1):
			urldict[year] = self.get_year(year)
		return urldict
