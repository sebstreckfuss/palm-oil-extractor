from urllib import request
from palmoilextractor.get.generator import UrlGenerator
from palmoilextractor.util import printProgressBar
from shutil import rmtree
from os import mkdir, path
import time

class Getter:
    def __init__(self, initial_dir="reports/", override=False):
        self.dir = initial_dir
        self.getter = UrlGenerator()
        try:
            mkdir(self.dir)
        except FileExistsError as e:
            if override:
                rmtree(self.dir)
                mkdir(self.dir)
            else:
                raise e
        except PermissionError as e:
            if override:
                rmtree(self.dir)
                mkdir(self.dir)
            else:
                raise e
        
    def get_year(self, year):
        print("\nDownloading reports from %s" % year)
        urls = self.getter.get_year(year)
        print()
        for url in urls:
            printProgressBar(urls.index(url)+1, len(urls)-1, "Downloading report %s:" % (url[1]), "/%s" % len(urls), 0, 10)
            request.urlretrieve(url[0], self.dir + url[1])

    def close(self):
        rmtree(self.dir)


if __name__=="__main__":
    getter = Getter(override=True)
    curr_year = int(time.strftime("%Y"))
    for year in range(2006, curr_year + 1):
        getter.get_year(year)

