from requests import post
from bs4 import BeautifulSoup
from datetime import datetime
from palmoilextractor.engine.processors import FullDate, ProductFormat

class SoupGetter:
    def __init__(self, method=print):
        self.method = method

    def get_soup(self, year, month):
        payload = {
            "tahun": year,
            "bulan": month,
            "submit22": "view+price"
        }
        self.soup = BeautifulSoup(
            post(
                "http://bepi.mpob.gov.my/admin2/price_local_daily_view.php",
                data=payload
            ).text,
            "lxml"
        )
        return self.soup

    def get_last_updated(self):
        return datetime.strptime(self.soup.select("p")[-1].text.split(":")[1].split()[0], "%d/%m/%Y")

    def get_product_names(self):
        trs = self.soup.select("tr")
        return [p.text.split("(")[0] for p in trs[1].select("td")[1:]]

    def get_date(self, y, x):
        day = datetime.strptime(y, "%d").strftime("%d")
        month = datetime.strptime(x.split()[0], "%b").strftime("%b")
        year = datetime.strptime("20{0}".format(x.split()[1]), "%Y").strftime("%Y")
        date = "{0}/{1}/{2}".format(day, month, year)
        return date

    def run(self, year, month):
        self.soup = self.get_soup(year, month)
        make_date = FullDate(None)
        product_format = ProductFormat(None)
        tenors = {
            0: "Spot",
            1: "M1",
            2: "M2",
            3: "M3"
        }
        products = self.get_product_names()
        trs = self.soup.select("tr")
        x = [t.text.replace("'", "") for t in trs[2].select("td")[1:]]
        y = [t.select("td")[0].text for t in trs[2:]][1:-1] # The last -1 is to remove the avg

        data = [[t.text for t in tr.select("td")[1:]] for tr in trs[3:]]

        for i, product in enumerate(products):
            product = product.strip()
            for _y in range(len(y)):
                for _x in range(4):

                    date = self.get_date(y[_y], x[0])
                    # Price data is just indexing a multi dimentional array that we generated
                    # earlier.
                    tenor = tenors[_x]
                    price = data[_y][_x + (4 * i)].replace(",", "_").strip()
                    # Add to db
                    if price in ["PH", "NT", "-", "", None]:
                        null_type = price
                        self.method(product_format.process(product), make_date.process(date.split("/")), "NULL", null_type, tenor)
                    else:
                        try:
                            self.method(product_format.process(product), make_date.process(date.split("/")), float(price.split("*")[0]), "NULL", tenor)
                        except ValueError:
                            print(price)

    def range_scrape(self, start_year, start_month, end_year, end_month):
        for year in range(start_year, end_year+1):
            for month in range(start_month, end_month+1):
                self.run(year, month)


