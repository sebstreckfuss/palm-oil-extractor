from mysql.connector import Connect
from mysql.connector.errors import InterfaceError, ProgrammingError
from palmoilextractor.database.inserter import Inserter
from palmoilextractor.database.update import Updater
from palmoilextractor.database.cursor import Cursor
from palmoilextractor.database.exceptions import ExecutionReturn

import re
from palmoilextractor.util import test_exception_conditions


class Database:
    def __init__(self, host, port, user, password, date=None, *args, **kwargs, ):
        print("Attempting to connect to %s as user %s" % (host, user))
        try:
            self.connection = Connect(host=host, port=port, user=user, password=password, *args, **kwargs)
        except InterfaceError as e:
            exit(print("Could not connect to %s\nReason:\n" % (host), e.msg))
        except ProgrammingError as e:
            if e.errno == 1044:
                exit(print("Disconnected from %s\nReason:\n\tAccess denied for user %s" % (host, user)))
            elif e.errno == 1045:
                exit(print("Could not connect to %s\nReason:\n\tIncorrect password for user %s" % (host, user)))
            else:
                raise e

        self.cursor = Cursor(self.connection)
        self.connection.autocommit = True
        self.insert = Inserter(self.connection, self.cursor)
        if date:
            self.update = Updater(self.connection, self.cursor, self.insert, date)

        try:
            self.cursor.execute("USE reports")
        except ProgrammingError as e:
            if e.errno in [1044, 1045]:
                exit(print("Disconnected from %s\nReason:\n\tAccess denied for user %s" % (host, user)))
            self.create()
        print("Successfully connected to %s as user %s" % (host, user))

    def create(self):
        print("Creating new database instance...")
        self.cursor.execute("CREATE DATABASE reports")
        self.cursor.execute("USE reports")
        for script in open("database/create_instance.sql").readlines():
            if re.search("(?<=^CREATE TABLE ).*?(?=( \())", script):
                print("Creating table " + re.search("(?<=^CREATE TABLE ).*?(?=( \())", script).group())
            self.cursor.execute(script)

    def query(self, qstring):
        """Execute command that returns a value or set"""
        self.cursor.execute(qstring)
        return self.cursor.fetchall()

    def execute(self, string):
        """Execute command that does not return anything"""
        self.cursor.execute(string)
        if not test_exception_conditions(self.cursor.fetchall, "errno", "-1", exception=InterfaceError):
            raise ExecutionReturn("The query returned a value", string)
