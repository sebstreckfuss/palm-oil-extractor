class ReportNotFoundError(Exception):
	pass

class ProductNotFoundError(Exception):
	pass

class RegionNotFoundError(Exception):
	pass

class SectorNotFoundError(Exception):
	pass

class CountryNotFoundError(Exception):
	pass

class PortNotFoundError(Exception):
	pass

class MonthError(Exception):
	pass

class ExecutionReturn(Exception):
	def __init__(self, msg, command):
		self.msg = msg
		self.command = command

	def __str__(self):
		return self.msg + ": " + self.command