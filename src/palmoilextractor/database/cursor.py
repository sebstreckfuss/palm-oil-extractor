from mysql.connector.cursor import MySQLCursor
from re import subn


class Cursor(MySQLCursor):

    def __init__(self, db_conn):
        super().__init__(db_conn)

    def execute(self, query, params=None, multi=False):
        if params:
            query = query % params
        patt1 = """('null')|("null")|('NULL')|("NULL")"""
        patt2 = """('nan')|("nan")|('NAN')|("NAN")"""
        query = subn(patt1, "NULL", query)
        query = subn(patt2, "0", query[0])[0]
        try:
            super().execute(query, params=params, multi=False)
        except Exception as e:
            print(query)
            raise e
