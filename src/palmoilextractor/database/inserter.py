from palmoilextractor.database.exceptions import *
from mysql.connector.errors import ProgrammingError
import re

class Inserter:
	def __init__(self, connection, cursor):
		self.connection = connection
		self.cursor = cursor

	def is_updater():
		return False
	
	def is_inserter():
		return True
	
	def get_product_id(self, product_name):
		self.cursor.execute("SELECT product_id FROM product WHERE product_name = '%s'" % (product_name))
		try:
			return self.cursor.fetchall()[0][0]
		except IndexError:
			raise ProductNotFoundError

	def get_region_id(self, region_name):
		self.cursor.execute("SELECT region_id FROM region WHERE region_name = '%s'" % (region_name))
		try:
			return self.cursor.fetchall()[0][0]
		except IndexError:
			raise RegionNotFoundError

	def get_sector_id(self, sector_name):
		self.cursor.execute("SELECT sector_id FROM sector WHERE sector_name = '%s'" % (sector_name))
		try:
			return self.cursor.fetchall()[0][0]
		except IndexError:
			raise SectorNotFoundError

	def get_country_id(self, country_name):
		try:
			self.cursor.execute("SELECT country_id FROM country WHERE country_name = '%s'" % country_name.replace("'", r"\'"))
		except ProgrammingError:
			raise RuntimeError
		try:
			return self.cursor.fetchall()[0][0]
		except IndexError:
			raise CountryNotFoundError

	def get_port_id(self, port_name):
		self.cursor.execute("SELECT port_id FROM port WHERE port_name = '%s'" % (port_name))
		try:
			return self.cursor.fetchall()[0][0]
		except IndexError:
			raise PortNotFoundError

	def check_report(self, report_id):
		self.cursor.execute("SELECT report_id FROM report WHERE report_id = '%s'" % (report_id))
		if not self.cursor.fetchall(): raise ReportNotFoundError

	def product(self, product_name):
		self.cursor.execute("INSERT INTO product(product_name) VALUES('%s')" % (product_name))

	def region(self, region_name):
		self.cursor.execute("INSERT INTO region(region_name) VALUES('%s')" % (region_name))

	def report(self, report_id, description):
		self.cursor.execute("INSERT INTO report(report_id, description) VALUES('%s', '%s')" % (report_id, description))

	def sector(self, sector_name):
		self.cursor.execute("INSERT INTO sector(sector_name) VALUES('%s')" % (sector_name))

	def country(self, name):
		self.cursor.execute("INSERT INTO country(country_name) VALUES('%s')" % name.replace("'", r"\'"))

	def port(self, name):
		self.cursor.execute("INSERT INTO port(port_name) VALUES('%s')" % (name))

	def daily_average(self, product_name, updated, average_amount, null_type, tenor):
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		if null_type == "NULL":
			self.cursor.execute("INSERT INTO daily_average(product_id, updated, average_amount, tenor) VALUES ('%s', '%s', '%s', '%s')"
				% (p_id, updated, average_amount, tenor))
		elif average_amount == "NULL":
			self.cursor.execute("INSERT INTO daily_average(product_id, updated, null_type, tenor) VALUES ('%s', '%s', '%s', '%s')"
				% (p_id, updated, null_type,  tenor))

	def production(self, product_name, report_id, quantity, produced, region_name='NULL'):
		self.check_report(report_id)
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		r_id = region_name
		if region_name != 'NULL':
			try:
				r_id = self.get_region_id(region_name)
			except RegionNotFoundError:
				self.region(region_name)
				r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO production(product_id, region_id, report_id, quantity, produced) VALUES('%s', '%s', '%s', '%s', '%s')"
			% (p_id, r_id, report_id, quantity, produced))

	def stock(self, product_name, report_id, month, quantity, region_name='NULL'):
		self.check_report(report_id)
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		r_id = region_name
		if region_name != 'NULL':
			try:
				r_id = self.get_region_id(region_name)
			except RegionNotFoundError:
				self.region(region_name)
				r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO stock(product_id, region_id, report_id, month, quantity) VALUES('%s', '%s', '%s', '%s', '%s')"
			% (p_id, r_id, report_id, month, quantity))

	def process(self, product_name, report_id, sector_name, month, processed):
		self.check_report(report_id)
		try:
			s_id = self.get_sector_id(sector_name)
		except SectorNotFoundError:
			self.sector(sector_name)
			s_id = self.get_sector_id(sector_name)
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		self.cursor.execute("INSERT INTO process(product_id, sector_id, report_id, month, quantity) VALUES ('%s', '%s', '%s', '%s', '%s')"
			% (p_id, s_id, report_id, month, processed))

	def refinery(self, product_name, region_name, report_id, month, processed):
		self.check_report(report_id)
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		try:
			r_id = self.get_region_id(region_name)
		except RegionNotFoundError:
			self.region(region_name)
			r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO refinery(product_id, region_id, report_id, month, processed) VALUES ('%s', '%s', '%s', '%s', '%s')"
			% (p_id, r_id, report_id, month, processed))

	def recovery_rate(self, region_name, report_id, month, recovery_rate):
		self.check_report(report_id)
		try:
			r_id = self.get_region_id(region_name)
		except RegionNotFoundError:
			self.region(region_name)
			r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO recovery_rate(region_id, report_id, month, recovery_rate) VALUES ('%s', '%s', '%s', '%s')"
			% (r_id, report_id, month, recovery_rate))

	def extraction(self, region_name, report_id, month, extraction):
		self.check_report(report_id)
		try:
			r_id = self.get_region_id(region_name)
		except RegionNotFoundError:
			self.region(region_name)
			r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO extraction(region_id, report_id, month, extraction) VALUES ('%s', '%s', '%s', '%s')"
			% (r_id, report_id, month, extraction))

	def import_(self, product_name, month, quantity):
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		self.cursor.execute("INSERT INTO import(product_id, month, quantity) VALUES ('%s', '%s', '%s')"
			% (p_id, month, quantity))

	def export(self, product_name, month, quantity, unit):
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		self.cursor.execute("INSERT INTO export(product_id, month, quantity, unit) VALUES ('%s', '%s', '%s', '%s')"
			% (p_id, month, quantity, unit))

	def export_country(self, country_name, month, product_name, quantity):
		try:
			c_id = self.get_country_id(country_name)
		except CountryNotFoundError:
			self.country(country_name)
			c_id = self.get_country_id(country_name)
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		self.cursor.execute("INSERT INTO export_country(country_id, product_id, month, quantity) VALUES ('%s', '%s', '%s', '%s')"
			% (c_id, p_id, month, quantity))

	def export_port(self, port_name, month, product_name, quantity):
		try:
			po_id = self.get_port_id(port_name)
		except PortNotFoundError:
			self.port(port_name)
			po_id = self.get_port_id(port_name)
		try:
			p_id = self.get_product_id(product_name)
		except ProductNotFoundError:
			self.product(product_name)
			p_id = self.get_product_id(product_name)
		self.cursor.execute("INSERT INTO export_port(port_id, product_id, month, quantity) VALUES ('%s', '%s', '%s', '%s')"
			% (po_id, p_id, month, quantity))

	def sector_capacity(self, sector_name, region_name, no, capacity, year):
		try:
			s_id = self.get_sector_id(sector_name)
		except SectorNotFoundError:
			self.sector(sector_name)
			s_id = self.get_sector_id(sector_name)
		try:
			r_id = self.get_region_id(region_name)
		except RegionNotFoundError:
			self.region(region_name)
			r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO sector_capacity(sector_id, region_id, no, capacity, year) VALUES ('%s', '%s', '%s', '%s', '%s')"
			% (s_id, r_id, no, capacity, year))
		self.connection.commt()

	def sector_usage(self, sector_name, report_id, month, utilization):
		self.check_report(report_id)
		try:
			s_id = self.get_sector_id(sector_name)
		except SectorNotFoundError:
			self.sector(sector_name)
			s_id = self.get_sector_id(sector_name)
		self.cursor.execute("INSERT INTO sector_usage(sector_id, report_id, month, utilization) VALUES ('%s', '%s', '%s', '%s')"
			% (s_id, report_id, month, utilization))

	def demand(self, month, region_name, amount):
		try:
			r_id = self.get_region_id(region_name)
		except RegionNotFoundError:
			self.region(region_name)
			r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO demand(month, region_id, amount) VALUES ('%s', '%s', '%s')"
			% (month, r_id, amount))

	def mill_received(self, region_name, month, received):
		try:
			r_id = self.get_region_id(region_name)
		except RegionNotFoundError:
			self.region(region_name)
			r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO mill_received(region_id, month, received) VALUES ('%s', '%s', '%s')"
			% (r_id, month, received))

	def mill_processed(self, region_name, month, processed):
		try:
			r_id = self.get_region_id(region_name)
		except RegionNotFoundError:
			self.region(region_name)
			r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO mill_processed(region_id, month, processed) VALUES ('%s', '%s', '%s')"
			% (r_id, month, processed))

	def mill_util(self, region_name, month, utilization):
		try:
			r_id = self.get_region_id(region_name)
		except RegionNotFoundError:
			self.region(region_name)
			r_id = self.get_region_id(region_name)
		self.cursor.execute("INSERT INTO mill_util(region_id, month, utilization) VALUES ('%s', '%s', '%s')"
			% (r_id, month, utilization)
		)