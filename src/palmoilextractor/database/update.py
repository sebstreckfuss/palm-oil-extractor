from json import load
import re

from palmoilextractor.database.inserter import Inserter

class Updater:
	def __init__(self, db_conn, cursor, insert, date):
		self.inserter = insert
		self.cursor = cursor
		if len(str(date[1])) != 1:
			self.date = "%s-%s-00" % date
		else:
			self.date = "%s-0%s-00" % date
		self._re = self.date + "?"
		print(self._re)
		with open("database/update_template.json") as j:
			self.templates = load(j)["templates"]
		for table in self.templates:
			setattr(self, table, lambda **kwargs: Updater.update(self, table=table, **kwargs))

	def is_updater():
		return True
	
	def is_inserter():
		return False

	def update(self, *args, **kwargs):
		d = [kwargs.get("month"), kwargs.get("date"), kwargs.get("year"), kwargs.get("produced")]
		if not any(re.match(self._re, date) for date in d if date):
			return
		p_id = self.inserter.get_product_id(kwargs.get("product_name")) \
		if kwargs.get("product_name")									\
		else "NULL"
		r_id = self.inserter.get_region_id(kwargs.get("region_name")) 	\
		if kwargs.get("region_name")									\
		else "NULL"
		c_id = self.inserter.get_country_id(kwargs.get("country_name")) \
		if kwargs.get("country_name")								 	\
		else "NULL"
		s_id = self.inserter.get_sector_id(kwargs.get("sector_name")) 	\
		if kwargs.get("sector_name")								 	\
		else "NULL"
		po_id = self.inserter.get_port_id(kwargs.get("port_name")) 		\
		if kwargs.get("port_name") 										\
		else "NULL"
		self.cursor.execute(self.templates[kwargs.get("table")].format(
			# IDs
			product_id=p_id, 
			region_id=r_id, 
			report_id=kwargs.get("report_id"), 
			sector_id=s_id,
			country_id=c_id,
			port_id=po_id,
			# Dates
			month=kwargs.get("month"), 
			date=kwargs.get("date"),
			year=kwargs.get("year"),
			produced=kwargs.get("produced"),
			# Cell data
			quantity=kwargs.get("quantity", 0),
			processed=kwargs.get("processed", 0),
			received=kwargs.get("received", 0),
			amount=kwargs.get("amount", 0),
			extraction=kwargs.get("extraction", 0),
			capacity=kwargs.get("capacity", 0),
			utilization=kwargs.get("utilization", 0),
			# Other
			no=kwargs.get("no", "NULL"),
			unit=kwargs.get("unit", "NULL")
			))